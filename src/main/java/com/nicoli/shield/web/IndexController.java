package com.nicoli.shield.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootVersion;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.nicoli.shield.beans.SpringSecurityInfoBean;


@Controller
public class IndexController {
  private static final Log LOG = LogFactory.getLog(IndexController.class);

  @Autowired
  SpringSecurityInfoBean securityInfoBean;

  @GetMapping("/")
  public String serveSamplePage(Model model) {
    LOG.info("Handling the request in IndexController @GetMapping '/'");

    model.addAttribute("springBootVersion", SpringBootVersion.getVersion());
    model.addAttribute("securityInfoBean", securityInfoBean);
    securityInfoBean.logAuthenticationDetails();

    return "sample/page";
  }
}
