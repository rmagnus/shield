package com.nicoli.shield.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;


/**
 * Configure the HTTP Spring Security mechanism, which almost always includes
 * some kind of form authentication. This class w/ its configuration 'behavior'
 * allows for an anonymous user to authenticate to us that they are a trusted
 * user because they have some form of credentialing that we have administered
 * proving we can trust them to access secure resources.
 *
 * <p>Here we are extending {@link WebSecurityConfigurerAdapter} base class and
 * overriding individual method...
 */
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
  private final String[] STATIC_FILES = new String[] { "/_*/**", "/favicon.ico" };
  private final String[] HOME_MAPPING = new String[] { "/", "/login", "/error" };

  /**
   * Great idea by Nick. Replace this with a database equivalent/OAuth AWS etc.
   */
  @Bean
  @Override
  public UserDetailsService userDetailsService() {
    @SuppressWarnings("deprecation")
    UserDetails user = User.withDefaultPasswordEncoder()
        .username("username")
        .password("password")
        .roles("SECRET_AGENT")
        .build();

    return new InMemoryUserDetailsManager(user);
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
      .authorizeRequests()
        .antMatchers(STATIC_FILES).permitAll()
        .antMatchers(HOME_MAPPING).permitAll()
        .and()
      .formLogin()
        .loginPage("/login")
        .permitAll()
        .and()
      .logout()
        .permitAll()
        .and()
      .csrf()
        .disable();


    /**
     * Example of something to which to aspire...?
     *
     * final AuthenticationSuccessHandler AUTH_GOOD = new SimpleUrlAuthenticationSuccessHandler("/dash");
     * final AuthenticationFailureHandler AUTH_BAD = new SimpleUrlAuthenticationFailureHandler("/bad");
     *
     * http
     *   .csrf()
     *     .disable()
     *   .authorizeRequests()
     *     .antMatchers(ANON).permitAll()
     *     .antMatchers(TEST).permitAll()
     *     .antMatchers(STAT).permitAll()
     *     .anyRequest().authenticated()
     *     .and()
     *   .formLogin()
     *     .loginProcessingUrl("/j_security_check")
     *     .usernameParameter("j_username")
     *     .passwordParameter("j_password")
     *     .successHandler(AUTH_GOOD)
     *     .failureHandler(AUTH_BAD)
     *     .loginPage("/login")
     *     .permitAll();
     */
  }
}
