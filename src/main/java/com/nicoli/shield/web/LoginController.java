package com.nicoli.shield.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.SpringVersion;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.nicoli.shield.beans.SpringSecurityInfoBean;


@Controller
public class LoginController {
  private static final Log LOG = LogFactory.getLog(LoginController.class);

  @Autowired
  SpringSecurityInfoBean securityInfoBean;

  @GetMapping("/login")
  public String serverLoginPage(Model model) {
    LOG.info("Handling the request in LoginController @GetMapping '/login'");

    model.addAttribute("springVersion", SpringVersion.getVersion());
    model.addAttribute("securityInfoBean", securityInfoBean);
    securityInfoBean.logAuthenticationDetails();

    return "login/page";
  }
}
