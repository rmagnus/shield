package com.nicoli.shield.web;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


@Controller
@Secured("ROLE_SECRET_AGENT")
public class SecuredController {

  @GetMapping("/secured")
  public String serveOnlyIfAuthenticatedWithRole() {
    return "sample/secret";
  }
}
