package com.nicoli.shield.beans;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;


@Component("securityInfoBean")
public class SpringSecurityInfoBean {
  private static final Log LOG = LogFactory.getLog(SpringSecurityInfoBean.class);

  public void logAuthenticationDetails() {
    SecurityContext ctx = SecurityContextHolder.getContext();
    LOG.info(buildSecurityContextDetails(ctx, false));
  }

  public String getSecurityContextDetails() {
    SecurityContext ctx = SecurityContextHolder.getContext();
    return buildSecurityContextDetails(ctx, true);
  }

  private String buildSecurityContextDetails(SecurityContext ctx, boolean markup) {
    String lineFeed = markup ? "<br/>" : StringUtils.LF;

    String mem = super.toString();
    StringBuilder info = new StringBuilder(mem.substring(mem.lastIndexOf(".") + 1));
    if (!markup) {
      info.append(lineFeed).append("--------------------------------------------");
      info.append(lineFeed).append("IndexController Security Context Holder Info");
      info.append(lineFeed).append("--------------------------------------------");
    }
    info.append(lineFeed);

    WebAuthenticationDetails details = (WebAuthenticationDetails)ctx.getAuthentication().getDetails();
    info.append(lineFeed);
    info.append(lineFeed).append("WebAuthenticationDetails object");
    info.append(lineFeed).append("~-~-~-~-~-~-~-~-~-~-~~-~-~-~-~-");
    info.append(lineFeed).append("  http sessionId: ").append(details.getSessionId());
    info.append(lineFeed).append("  remote address: ").append(details.getRemoteAddress());
    info.append(lineFeed);

    Authentication auth = ctx.getAuthentication();
    info.append(lineFeed);
    info.append(lineFeed).append("Authentication object");
    info.append(lineFeed).append("~-~-~-~-~-~-~-~-~-~-~");
    info.append(lineFeed).append("  principalType: ").append(auth.getPrincipal().getClass().getSimpleName());
    info.append(lineFeed).append("      principal: ").append(auth.getPrincipal());
    info.append(lineFeed).append("   authoritites: ").append(auth.getAuthorities());
    info.append(lineFeed);

    return markup ? info.toString().replaceAll(" ", "\u00a0") : info.toString();
  }
}
