package com.nicoli.shield.config;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.nicoli.shield.ShieldApplication;


/**
 * This class is here to ensure that your SpringApplication will work under a
 * traditional WAR deployment, web application environment. With this class in
 * place, when your application is deployed as a WAR to a servlet container such
 * as Tomcat, it will initialize and work like a traditional Java web application.
 */
public class WarFileInitializer extends SpringBootServletInitializer {

  /**
   * When you tell the passed builder about what Java sources to use, you are
   * telling it to use ShieldApplication as the driver for all things configuration
   * related. So that class really is the starting point for a Spring Boot app!
   */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ShieldApplication.class);
	}

}
