package com.nicoli.shield;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


/**
 * This starts up the application (see run) and triggers auto configuration of
 * your app as well as component scanning. This is the equivalent of declaring
 * {@code @Configuration}, {@code @EnableAutoConfiguration} and {@code @ComponentScan}.
 */
@SpringBootApplication
@ComponentScan(basePackages = { "com.nicoli.shield" })
public class ShieldApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShieldApplication.class, args);
	}
}
