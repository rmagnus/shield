package com.nicoli.shield.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;


@EnableWebMvc
@Configuration
public class WebResourceConfig implements WebMvcConfigurer {

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry.addResourceHandler("/_css/**").addResourceLocations("classpath:/static/css/");
    registry.addResourceHandler("/_img/**").addResourceLocations("classpath:/static/img/");
    registry.addResourceHandler("/_js/**").addResourceLocations("classpath:/static/js/");
  }
  
  @Bean
  public FreeMarkerConfigurer freemarkerConfig() { 
    FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer(); 
    freeMarkerConfigurer.setTemplateLoaderPath("classpath:/templates");
    return freeMarkerConfigurer; 
  }

  @Bean
  public FreeMarkerViewResolver freemarkerViewResolver() { 
    FreeMarkerViewResolver resolver = new FreeMarkerViewResolver(); 
    resolver.setCache(true);
    resolver.setPrefix(""); 
    resolver.setSuffix(".fm"); 
    return resolver; 
  }
}
